import React from "react";
import { useForm, ValidationError } from "@formspree/react";

function ContactForm() {
  const [state, handleSubmit] = useForm("xknyqgpp");
  if (state.succeeded) {
    return <p>Danke für ihre Nachricht!</p>;
  }
  return (
    <div className={"contact"}>
      <form onSubmit={handleSubmit}>
        <label htmlFor="email">E-Mail Adresse</label>
        <input id="email" type="email" name="email" />
        <ValidationError prefix="Email" field="email" errors={state.errors} />
        <label htmlFor="email">Nachricht</label>
        <textarea id="message" name="message" />
        <ValidationError
          prefix="Message"
          field="message"
          errors={state.errors}
        />
        <button type="submit" disabled={state.submitting}>
          Absenden
        </button>
      </form>
    </div>
  );
}
export default ContactForm;
