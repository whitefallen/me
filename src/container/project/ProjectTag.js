import React from "react";

function ProjectTag({ tag }) {
  return <span className={"project-card-tag"}>{tag}</span>;
}
export default ProjectTag;
